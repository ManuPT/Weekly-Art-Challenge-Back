package com.m2i.wac.models.dtos;

import java.util.ArrayList;

import com.m2i.wac.models.Category;
import com.m2i.wac.models.Tag;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SortDTO {
	private Category category;
	private ArrayList<Tag> tags;
}
