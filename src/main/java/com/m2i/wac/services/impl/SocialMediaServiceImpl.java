package com.m2i.wac.services.impl;

import com.m2i.wac.models.SocialMedia;
import com.m2i.wac.repositories.SocialMediaRepository;
import com.m2i.wac.services.SocialMediaService;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

public class SocialMediaServiceImpl implements SocialMediaService {

    private SocialMediaRepository socialMediaRepository;

    public SocialMediaServiceImpl(SocialMediaRepository socialMediaRepository) {
        this.socialMediaRepository = socialMediaRepository;
    }

    @Override
    public List<SocialMedia> getAll() {  return this.socialMediaRepository.findAll(); }

    @Override
    public SocialMedia getById(Long id) { return this.socialMediaRepository.findById(id).orElseThrow(
            ()-> new ResponseStatusException(HttpStatus.NOT_FOUND)); }

    @Override
    public SocialMedia postSocialMedia(SocialMedia socialMedia) { return this.socialMediaRepository.save(socialMedia); }

    @Override
    public SocialMedia putSocialMedia(SocialMedia socialMedia) { return this.socialMediaRepository.save(socialMedia); }

    @Override
    public void delete(Long id) { this.socialMediaRepository.deleteById(id); }
}
