package com.m2i.wac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WacApplication {

	public static void main(String[] args) {
		SpringApplication.run(WacApplication.class, args);
	}

}
