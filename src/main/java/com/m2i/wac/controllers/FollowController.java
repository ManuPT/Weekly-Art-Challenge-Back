package com.m2i.wac.controllers;

import com.m2i.wac.models.Follow;
import com.m2i.wac.services.FollowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("follows")
public class FollowController {
    @Autowired
    private FollowService followService;

    @GetMapping("{id}")
    public Follow findById(@PathVariable Long id) {
        return followService.getById(id);
    }

    @PostMapping
    public Follow postFollow(@RequestBody Follow follow) {
        return followService.postFollow(follow);
    }

    @PutMapping
    public Follow putFollow(@RequestBody Follow follow) {
        return followService.putFollow(follow);
    }

    @DeleteMapping("{id}")
    public void deleteFollow(@PathVariable Long id) {
        followService.deleteFollow(id);
    }

    @GetMapping
    public List<Follow> findAll() {
        return followService.findAll();
    }

    @PostMapping("/exist")
    public Boolean isAlreadyFollowed(@RequestBody Follow follow) {
        return this.followService.isAlreadyFollowed(follow);
    }

    @GetMapping("/followers/{id}")
    public List<Follow> findFollowsByUserId(@PathVariable Long id) {
        return this.followService.findFollowsByUserId(id);
    }

    @PostMapping("/delete")
    public Boolean deleteByUserIdAndUserFollowedId(@RequestBody Follow follow) {
        return this.followService.deleteByUserIdAndUserFollowedId(follow);
    }
}
