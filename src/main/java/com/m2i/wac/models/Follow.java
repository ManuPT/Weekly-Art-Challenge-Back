package com.m2i.wac.models;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;

import java.time.LocalDateTime;


/**
 * The persistent class for the follows database table.
 * 
 */
@Entity
@Table(name="follows")
@Data
public class Follow{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonProperty(access = Access.WRITE_ONLY)
	@CreationTimestamp
	@Column(name="date_create",updatable = false)
	private LocalDateTime dateCreate;

	@JsonProperty(access = Access.WRITE_ONLY)
	@UpdateTimestamp
	@Column(name="date_update")
	private LocalDateTime dateUpdate;

	@ManyToOne
	@JoinColumn(name="id_user")
	private User user;

	@ManyToOne
	@JoinColumn(name="id_user_followed")
	private User userFollowed;
}