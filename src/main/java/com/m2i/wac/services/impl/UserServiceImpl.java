package com.m2i.wac.services.impl;

import java.nio.charset.StandardCharsets;
import java.util.List;

import com.m2i.wac.models.Role;
import com.m2i.wac.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;

import com.google.common.hash.Hashing;
import com.m2i.wac.models.User;
import com.m2i.wac.models.dtos.UserLoginDTO;
import com.m2i.wac.repositories.UserRepository;

public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@Override
	public List<User> getAll() {
		return this.userRepository.findAll();
	}

	@Override
	public User getById(@PathVariable Long id) {
		return this.userRepository.findById(id).orElseThrow(
				()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	/**
	 * Inscription d'un nouvel utilisateur après hash de son mot de passe et attribution de son rôle de "user"
	 * @param user
	 * @return le user qui a été enregistré
	 */
	@Override
	public User post(User user) {
		user.setPassword(Hashing.sha256()
				  .hashString(user.getPassword(), StandardCharsets.UTF_8)
				  .toString());
		user.setRole(new Role(3L));
		return this.userRepository.save(user);
	}

	@Override
	public User put(User user) {
		user.setPassword(Hashing.sha256()
				  .hashString(user.getPassword(), StandardCharsets.UTF_8)
				  .toString());
		return this.userRepository.save(user);
	}

	@Override
	public void delete(@PathVariable Long id) {
		this.userRepository.deleteById(id);
	}

	@Override
	public User login(UserLoginDTO user) {
		User login = this.userRepository.findUserByPseudo(user.getPseudo()).orElseThrow(()->{
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		});
		String password = Hashing.sha256()
				  .hashString(user.getPassword(), StandardCharsets.UTF_8)
				  .toString();
		if(login.getPassword().equals(password)) return login;
		else throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}

}
