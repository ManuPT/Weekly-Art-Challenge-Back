package com.m2i.wac.services;

import java.util.List;

import com.m2i.wac.models.Art;

public interface ArtService {

	Art getById(Long id);

	Art postArt(Art art);

	Art putArt(Art art);

	void deleteArt(Long id);

	List<Art> getAll();

}
