package com.m2i.wac.repositories;

import com.m2i.wac.models.Like;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LikeRepository extends JpaRepository<Like, Long> {

	Boolean existsByUserIdAndArtId(Long userId, Long artId);

	Optional<Like> findByUserIdAndArtId(Long userId, Long artId);
}
