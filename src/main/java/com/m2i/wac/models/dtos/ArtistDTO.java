package com.m2i.wac.models.dtos;

import com.m2i.wac.models.Art;
import com.m2i.wac.models.SocialMedia;
import com.m2i.wac.models.Tag;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArtistDTO {

    private Long id;
    private String description;
    private String firstname;
    private String lastname;
    private String mail;
    private String pseudo;
    private List<Tag> tags;
    private List<Art> arts;
    private List<SocialMedia> socialMedias;
    private Integer nbParticipation;
    private Integer nbFollow;
    private Art soumission;

    public ArtistDTO(Long id, String description, String firstname, String lastname, String mail, String pseudo){
        this.id = id;
        this.description = description;
        this.firstname = firstname;
        this.lastname = lastname;
        this.mail = mail;
        this.pseudo = pseudo;
    }
}
