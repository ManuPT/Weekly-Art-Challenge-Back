package com.m2i.wac.models;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;

import java.time.LocalDateTime;


@Entity
@Table(name="reports")
@Data
public class Report{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonProperty(access = Access.WRITE_ONLY)
	@CreationTimestamp
	@Column(name="date_create",updatable = false)
	private LocalDateTime dateCreate;

	@JsonProperty(access = Access.WRITE_ONLY)
	@UpdateTimestamp
	@Column(name="date_update")
	private LocalDateTime dateUpdate;

	@ManyToOne
	@JoinColumn(name="id_art")
	private Art art;

	@ManyToOne
	@JoinColumn(name="id_user")
	private User user;

}