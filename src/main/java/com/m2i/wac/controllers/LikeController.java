package com.m2i.wac.controllers;

import com.m2i.wac.models.Like;
import com.m2i.wac.services.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("likes")
public class LikeController {

    @Autowired
    private LikeService likeService;

    @GetMapping
    public List<Like> getAll() { return this.likeService.getAll(); }

    @GetMapping("{id}")
    public Like getById(@PathVariable Long id) { return this.likeService.getById(id); }

    @PostMapping
    public Like postLike(@RequestBody Like like) { return this.likeService.postLike(like); }

    @PutMapping
    public Like putLike(@RequestBody Like like) { return this.likeService.putLike(like); }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) { this.likeService.delete(id); }
    
    @PostMapping("exists")
    public Boolean exists(@RequestBody Like like) {
    	return this.likeService.isAlreadyLiked(like);
    }
    
    @PostMapping("delete")
    public Boolean deleteByLike(@RequestBody Like like) {
    	return this.likeService.deleteByUserIdAndArtId(like);
    }
}
