package com.m2i.wac.services;

import com.m2i.wac.models.SocialMedia;

import java.util.List;

public interface SocialMediaService {
    public List<SocialMedia> getAll();

    public SocialMedia getById(Long id);

    public SocialMedia postSocialMedia(SocialMedia socialMedia);

    public SocialMedia putSocialMedia(SocialMedia socialMedia);

    public void delete(Long id);
}
