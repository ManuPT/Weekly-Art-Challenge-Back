package com.m2i.wac.controllers;


import com.m2i.wac.models.Art;
import com.m2i.wac.models.Tag;
import com.m2i.wac.models.Theme;
import com.m2i.wac.models.dtos.SortDTO;
import com.m2i.wac.services.ThemeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("themes")
public class ThemeController {

    @Autowired
    private ThemeService themeService;

    @GetMapping
    public List<Theme> getAll() {
        return themeService.getAll();
    }

    @GetMapping("{id}")
    public Theme getById(@PathVariable Long id) {
        return themeService.getById(id);
    }

    @PostMapping
    public Theme postTheme(@RequestBody Theme theme) {
        return themeService.postTheme(theme);
    }

    @PutMapping
    public Theme putTheme(@RequestBody Theme theme) {
        return themeService.putTheme(theme);
    }

    @DeleteMapping("{id}")
    public void deleteTheme(@PathVariable Long id) {
        themeService.deleteTheme(id);
    }

    /**
     * Retourne la liste des Art d'un Theme
     * @param id
     * @return
     */
    @GetMapping("{id}/arts")
    public List<Art> getArtByThemeId(@PathVariable Long id){
        return this.themeService.getArtByThemeId(id);
    }

    //////////////////
    // Routes concernant le thème suivant
    //////////////////

    /**
     * Retourne le Theme en état de soumission
     * @return
     */
    @GetMapping("next")
    public Theme getNextTheme() {
        return this.themeService.getNextTheme();
    }

    /**
     * Retourne les Art du Theme en état de soumission
     * @return
     */
    @GetMapping("next/arts")
    public List<Art> getArtByNextTheme(){
    	return this.themeService.getArtByNextTheme();
    }
    
    ///////////////
    // 	Routes concernant le thème à voter actuellement
    ///////////////

    /**
     * Retourne le Theme en état de vote
     * @return
     */
    @GetMapping("current")
    public Theme getCurrentTheme() {
        return this.themeService.getCurrentTheme();
    }

    /**
     * Retourne les Art du Theme en état de vote
     * @return
     */
    @GetMapping("current/arts")
    public List<Art> getArtByCurrentVoteTheme(){
    	return this.themeService.getArtByCurrentTheme();
    }

    /**
     * Retourne les Art du Theme en état de vote selon les Artistes suivis
     * @param idUser
     * @return
     */
    @GetMapping("current/arts/followed/{idUser}")
    public List<Art> getArtByCurrentVoteThemeAndFollowedByUserId(@PathVariable Long idUser){
    	return this.themeService.getArtByCurrentVoteThemeAndFollowedByUserId(idUser);
    }

    /**
     * Retourne les Art du Theme en état de vote qui sont le plus votées sur la journée
     * @return
     */
    @GetMapping("current/arts/daily")
    public List<Art> getArtByCurrentVoteThemeMostLikedToday(){
    	return this.themeService.getArtByCurrentVoteThemeMostLikedToday();
    }
    
    /////////
    //	Routes concernant les résultats du dernier thème
    /////////

    /**
     * Retourne le dernier Theme en état de résultat
     * @return
     */
    @GetMapping("last")
    public Theme getLastTheme() {
        return this.themeService.getLastTheme();
    }

    /**
     * Retourne les Art du dernier Theme en état de résultat
     * @return
     */
    @GetMapping("last/arts")
    public List<Art> getArtByLastTheme(){
    	return this.themeService.getArtByLastTheme();
    }

    /**
     * Retourne les Art du dernier Theme en état de résultat selon les Artistes suivis
     * @param idUser
     * @return
     */
    @GetMapping("last/arts/followed/{idUser}")
    public List<Art> getArtByLastThemeAndFollowedByUserId(@PathVariable Long idUser){
    	return this.themeService.getArtByLastThemeAndFollowedByUserId(idUser);
    }

    /**
     * Retourne le top 10 des Art du dernier Theme en état de résultat en fonction d'une catégorie
     * @param idCategory
     * @return
     */
    @GetMapping("last/arts/categories/{idCategory}/top10")
    public List<Art> getArtByLastThemeAndTop10ByIdCategory(@PathVariable Long idCategory){
    	return this.themeService.getArtByLastThemeAndTop10ByIdCategory(idCategory);
    }
    
    /**
     * Retour une liste des Art suivant le Theme, la Category (optionnelle) , et une List de Tags
     * @param id
     * @param sort
     * @return
     */
    @PostMapping("{id}/arts/sort")
    public HashSet<Art> getArtByThemeIdAndSortDTO(@PathVariable Long id,@RequestBody SortDTO sort){
    	return this.themeService.getArtByThemeIdAndSortDTO(id,sort);
    }
    
    /**
     * Retourne la liste des Tags d'un thème suivant les Art posté dedans
     * @param id
     * @return
     */
    @GetMapping("{id}/arts/tags")
    public HashSet<Tag> getTagByThemeId(@PathVariable Long id){
    	return this.themeService.getTagByThemeId(id);
    }
}
