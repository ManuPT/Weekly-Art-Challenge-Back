package com.m2i.wac.services.impl;

import com.m2i.wac.models.Report;
import com.m2i.wac.repositories.ReportRepository;
import com.m2i.wac.services.ReportService;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

public class ReportServiceImpl implements ReportService {

    private ReportRepository reportRepository;

    public ReportServiceImpl(ReportRepository reportRepository) {
        this.reportRepository = reportRepository;
    }

    @Override
    public List<Report> getAll() { return this.reportRepository.findAll(); }

    @Override
    public Report getById(Long id) { return this.reportRepository.findById(id).orElseThrow(
            ()-> new ResponseStatusException(HttpStatus.NOT_FOUND)); }

    @Override
    public Report postReport(Report report) {
        return this.reportRepository.save(report);
    }

    @Override
    public Report putReport(Report report) {
        return this.reportRepository.save(report);
    }

    @Override
    public void delete(Long id) { this.reportRepository.deleteById(id); }
}
