package com.m2i.wac.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.m2i.wac.models.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

}
