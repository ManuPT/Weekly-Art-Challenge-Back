package com.m2i.wac.services.impl;

import com.m2i.wac.models.Art;
import com.m2i.wac.models.Follow;
import com.m2i.wac.models.Tag;
import com.m2i.wac.models.Theme;
import com.m2i.wac.models.dtos.SortDTO;
import com.m2i.wac.repositories.ArtRepository;
import com.m2i.wac.repositories.FollowRepository;
import com.m2i.wac.repositories.ThemeRepository;
import com.m2i.wac.services.ThemeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

public class ThemeServiceImpl implements ThemeService {

    private ThemeRepository themeRepository;
    private ArtRepository artRepository;
    private FollowRepository followRepository;

    public ThemeServiceImpl(ThemeRepository themeRepository,ArtRepository artRepository, FollowRepository followRepository) {
        this.themeRepository = themeRepository;
        this.artRepository = artRepository;
        this.followRepository = followRepository;
    }

    @Override
    public List<Theme> getAll() {
        return this.themeRepository.findAllByOrderByDateStartDesc();
    }

    @Override
    public Theme postTheme(Theme theme) {
        return this.themeRepository.save(theme);
    }

    @Override
    public Theme putTheme(Theme theme) {
        return this.themeRepository.save(theme);
    }

    @Override
    public Theme getById(Long id) {
        return this.themeRepository.findById(id).orElseThrow(
				()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public void deleteTheme(Long id) {
        this.themeRepository.deleteById(id);
    }

	@Override
	public Theme getNextTheme() {
		return this.themeRepository.getThemeByPeriodeId((long) 1);
	}

	@Override
	public Theme getCurrentTheme() {
		return this.themeRepository.getThemeByPeriodeId((long) 2);
	}

	@Override
	public Theme getLastTheme() {
		return this.themeRepository.findFirstThemeByPeriodeIdOrderByDateStartDesc((long)3);
	}

	@Override
	public List<Art> getArtByNextTheme() {
		return getArtByThemeId(getNextTheme().getId());
	}
    
	@Override
	public List<Art> getArtByCurrentTheme() {
		Theme theme = getCurrentTheme();
		if(theme==null) {
			return new ArrayList<Art>();
		}
		return getArtByThemeId(theme.getId());
	}

	@Override
	public List<Art> getArtByThemeId(Long id) {
		return this.artRepository.getArtByThemeId(id);
	}
	
	
	public List<Art> getArtByThemeIdAndCategoryId(Long idTheme,Long idCategory) {
		return this.artRepository.getArtByThemeIdAndCategoryId(idTheme,idCategory);
	}

	@Override
	public List<Art> getArtByLastTheme() {
		Theme theme = getLastTheme();
		return getArtByThemeId(theme.getId());
	}

	@Override
	public List<Art> getArtByCurrentVoteThemeAndFollowedByUserId(Long id) {
		Theme voteTheme = getCurrentTheme();
		return getArtByThemeIdAndFollowedByUserId(voteTheme.getId(),id);
	}

	@Override
	public List<Art> getArtByCurrentVoteThemeMostLikedToday() {
		List<Art> arts = getArtByCurrentTheme();
		arts.sort(Comparator.comparing(Art::getNbrLikesToday).reversed());
		if(arts.size()>10) arts = arts.subList(0, 10);
		return arts;
	}

	@Override
	public List<Art> getArtByLastThemeAndFollowedByUserId(Long id) {
		Theme voteTheme = getLastTheme();
		return getArtByThemeIdAndFollowedByUserId(voteTheme.getId(),id);
	}

	@Override
	public List<Art> getArtByLastThemeAndTop10ByIdCategory(Long idCategory) {
		Theme lastTheme = getLastTheme();
		List<Art> arts = getArtByThemeIdAndCategoryId(lastTheme.getId(),idCategory);
		arts.sort(Comparator.comparing(Art::getNbrLikes).reversed());
		if(arts.size()>10) arts = arts.subList(0, 10);
		return arts;
	}

	private List<Art> getArtByThemeIdAndFollowedByUserId(Long idTheme,Long idUser){
		List<Follow> resultFollow = this.followRepository.findByUserId(idUser);
		List<Art> resultArt = new ArrayList<>();
		resultFollow.forEach(follow->{
			Art art = this.artRepository.getArtByThemeIdAndUserId(idTheme,follow.getUserFollowed().getId());
			if(art!=null) resultArt.add(art);
		});
		
		return resultArt;
	}

	@Override
	public HashSet<Art> getArtByThemeIdAndSortDTO(Long id, SortDTO sort) {
		HashSet<Art> result = new HashSet<Art>();
		if(sort.getCategory()!=null) {
			sort.getTags().forEach((tag)->{
				result.addAll(this.artRepository.getArtByThemeIdAndCategoryIdAndTagsId(id,sort.getCategory().getId(),tag.getId()));
			});
		}
		else {
			sort.getTags().forEach((tag)->{
				result.addAll(this.artRepository.getArtByThemeIdAndTagsId(id,tag.getId()));
			});
		}
		return result;
	}

	@Override
	public HashSet<Tag> getTagByThemeId(Long id) {
		List<Art> result = getArtByThemeId(id);
		HashSet<Tag> tags = new HashSet<Tag>();
		result.forEach((art)->{
			tags.addAll(art.getTags());
		});
		return tags;
	}
}
