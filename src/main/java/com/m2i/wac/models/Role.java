package com.m2i.wac.models;

import javax.persistence.*;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;

import java.time.LocalDateTime;


@Entity
@Table(name="roles")
@Data
@NoArgsConstructor
public class Role{

	public Role(Long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonProperty(access = Access.WRITE_ONLY)
	@CreationTimestamp
	@Column(name="date_create",updatable = false)
	private LocalDateTime dateCreate;

	@JsonProperty(access = Access.WRITE_ONLY)
	@UpdateTimestamp
	@Column(name="date_update")
	private LocalDateTime dateUpdate;

	private String name;

}