package com.m2i.wac.repositories;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.m2i.wac.models.User;

public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findUserByPseudo(String pseudo);

}
