package com.m2i.wac.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.m2i.wac.models.Art;
import com.m2i.wac.services.ArtService;

@RestController
@CrossOrigin
@RequestMapping("/arts")
public class ArtController {
	@Autowired
	private ArtService artService;
	

	@GetMapping
	public List<Art> getAll(){
		return this.artService.getAll();
	}
	
	
	@GetMapping("{id}")
	public Art getById(@PathVariable Long id) {
		return this.artService.getById(id);
	}
	
	@PostMapping("")
	public Art postArt(@RequestBody Art art) {
		return this.artService.postArt(art);
	}
	
	@PutMapping("")
	public Art putArt(@RequestBody Art art) {
		return this.artService.putArt(art);
	}
	
	@DeleteMapping("{id}")
	public void deleteArt(@PathVariable Long id) {
		this.artService.deleteArt(id);
	}
}