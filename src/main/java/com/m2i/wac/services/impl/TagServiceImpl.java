package com.m2i.wac.services.impl;

import java.util.List;

import com.m2i.wac.models.Tag;
import com.m2i.wac.repositories.TagRepository;
import com.m2i.wac.services.TagService;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class TagServiceImpl implements TagService {
	
	private TagRepository tagRepository;
	
	public TagServiceImpl(TagRepository tagRepository) {
		this.tagRepository = tagRepository;
	}

	@Override
	public List<Tag> getAll() {
		return this.tagRepository.findAll();
	}

	@Override
	public Tag getById(Long id) {
		return this.tagRepository.findById(id).orElseThrow(
				()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	@Override
	public Tag post(Tag tag) {
		return this.tagRepository.getTagByName(tag.getName()).orElseGet(() -> this.tagRepository.save(tag));
	}

	@Override
	public Tag put(Tag tag) {
		return this.tagRepository.getTagByName(tag.getName()).orElseGet(() -> this.tagRepository.save(tag));
	}

	@Override
	public void delete(Long id) {
		this.tagRepository.deleteById(id);
	}

}
