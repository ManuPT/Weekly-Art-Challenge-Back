package com.m2i.wac.controllers;


import com.m2i.wac.models.Category;
import com.m2i.wac.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public List<Category> getAll(){
        return this.categoryService.getAll();
    }

    @GetMapping("{id}")
    public Category getById(@PathVariable Long id){
        return this.categoryService.getById(id);
    }

    @PostMapping
    public Category postCategory(@RequestBody Category category){
        return this.categoryService.postCategory(category);
    }

    @PutMapping
    public Category putCategory(@RequestBody Category category){
        return this.categoryService.putCategory(category);
    }

    @DeleteMapping("{id}")
    public void deleteCategory(@PathVariable Long id){
        categoryService.deleteCategory(id);
    }
}
