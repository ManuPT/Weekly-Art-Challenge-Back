package com.m2i.wac.controllers;


import com.m2i.wac.models.Art;
import com.m2i.wac.models.dtos.ArtistDTO;
import com.m2i.wac.services.ArtistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("artists")
public class ArtistController {

    @Autowired
    private ArtistService artistService;

    @GetMapping
    public List<ArtistDTO> getAll(){
        return this.artistService.getAll();
    }

    @GetMapping("{id}")
    public ArtistDTO getById(@PathVariable Long id){
        return this.artistService.getById(id);
    }

    @DeleteMapping("{id}")
    public void deleteArtist(@PathVariable Long id){
        this.artistService.deleteArtist(id);
    }

    @GetMapping("{id}/arts")
    public List<Art> getAllArts(@PathVariable Long id){
        return this.artistService.getAllArts(id);
    }

}
