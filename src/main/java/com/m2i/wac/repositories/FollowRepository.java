package com.m2i.wac.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.m2i.wac.models.Follow;

public interface FollowRepository extends JpaRepository<Follow, Long>{

	public List<Follow> findByUserId(Long id);
    public List<Follow> getFollowByUserFollowedId(Long id);

    Optional<Follow> findByUserIdAndUserFollowedId(Long userId, Long userFollowedId);

    Boolean existsByUserIdAndUserFollowedId(Long userId, Long userFollowedId);

    void deleteByUserIdAndUserFollowedId(Long userId, Long UserFollowedId);
}
