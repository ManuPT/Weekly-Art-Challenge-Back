package com.m2i.wac.models;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name="arts")
@Data
public class Art{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Lob
	private String content;

	@JsonProperty(access = Access.WRITE_ONLY)
	@CreationTimestamp
	@Column(name="date_create",updatable = false)
	private LocalDateTime dateCreate;

	@JsonProperty(access = Access.WRITE_ONLY)
	@UpdateTimestamp
	@Column(name="date_update")
	private LocalDateTime dateUpdate;

	private String name;

	@ManyToOne
	@JoinColumn(name="id_categorie")
	private Category category;

	@ManyToOne
	@JoinColumn(name="id_theme",updatable = false)
	private Theme theme;

	@ManyToOne
	@JoinColumn(name="id_auteur")
	private User user;

	@ManyToMany
	@JoinTable(
			name="has_tags"
			, joinColumns={
				@JoinColumn(name="id_art")
				}
			, inverseJoinColumns={
				@JoinColumn(name="id_tag")
				}
			)
	private List<Tag> tags;
	
	@OneToMany(mappedBy="art")
	@JsonIgnore
	private List<Like> likes;
	
	
	@Transient
	private Integer nbrLikes;
	
	public Integer getNbrLikes() {
		if(likes!=null&&!likes.isEmpty())
			return likes.size();
		else {
			return 0;
		}
	}
	
	@Transient
	private Integer nbrLikesToday;
	
	public Integer getNbrLikesToday() {
		nbrLikesToday=0;
		if(likes!=null&&!likes.isEmpty()) {
			likes.forEach((like)->{
				if(like.getDateCreate().toLocalDate().equals(LocalDate.now())) nbrLikesToday++;
			});
			return nbrLikesToday;
		}
		else {
			return 0;
		}
	}

}