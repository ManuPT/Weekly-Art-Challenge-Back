package com.m2i.wac.services.impl;

import java.util.List;

import com.m2i.wac.models.Art;
import com.m2i.wac.repositories.ArtRepository;
import com.m2i.wac.repositories.ThemeRepository;
import com.m2i.wac.services.ArtService;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ArtServiceImpl implements ArtService {
	
	private ArtRepository artRepository;
	private ThemeRepository themeRepository;
	
	public ArtServiceImpl(ArtRepository artRepository,ThemeRepository themeRepository) {
		this.artRepository = artRepository;
		this.themeRepository = themeRepository;
	}

	@Override
	public List<Art> getAll() {
		return this.artRepository.findAll();
	}
	
	@Override
	public Art getById(Long id) {
		return this.artRepository.findById(id).orElseThrow(
				()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	@Override
	public Art postArt(Art art) {
		art.setTheme(this.themeRepository.getThemeByPeriodeId((long)1));
		return this.artRepository.save(art);
	}

	@Override
	public Art putArt(Art art) {
		return this.artRepository.save(art);
	}

	@Override
	public void deleteArt(Long id) {
		this.artRepository.deleteById(id);
	}

}
