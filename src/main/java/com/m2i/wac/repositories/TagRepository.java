package com.m2i.wac.repositories;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.m2i.wac.models.Tag;

public interface TagRepository extends JpaRepository<Tag, Long> {

	Optional<Tag> getTagByName(String name);

}
