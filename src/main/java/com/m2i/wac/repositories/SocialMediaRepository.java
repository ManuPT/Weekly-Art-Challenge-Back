package com.m2i.wac.repositories;

import com.m2i.wac.models.SocialMedia;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SocialMediaRepository extends JpaRepository<SocialMedia, Long> {

    public List<SocialMedia> getSocialMediaByUserId(Long id);
}
