package com.m2i.wac.models;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;

import java.time.LocalDateTime;

@Entity
@Table(name="users")
@Data
public class User{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonProperty(access = Access.WRITE_ONLY)
	@CreationTimestamp
	@Column(name="date_create",updatable = false)
	private LocalDateTime dateCreate;

	@JsonProperty(access = Access.WRITE_ONLY)
	@UpdateTimestamp
	@Column(name="date_update")
	private LocalDateTime dateUpdate;

	@Lob
	private String description;

	private String firstname;

	private String lastname;

	private String mail;

	@JsonProperty(access = Access.WRITE_ONLY)
	private String password;

	private String pseudo;

	@ManyToOne
	@JoinColumn(name="id_role")
	private Role role;

}