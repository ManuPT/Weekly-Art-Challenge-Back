package com.m2i.wac.services.impl;

import com.m2i.wac.models.*;
import com.m2i.wac.models.dtos.ArtistDTO;
import com.m2i.wac.repositories.*;
import com.m2i.wac.services.ArtistService;
import com.m2i.wac.services.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAll() {
        return this.categoryRepository.findAll();
    }

    @Override
    public Category postCategory(Category category) {
        return this.categoryRepository.save(category);
    }

    @Override
    public Category putCategory(Category category) {
        return this.categoryRepository.save(category);
    }

    @Override
    public Category getById(Long id) {
        return this.categoryRepository.findById(id).orElseThrow(
                ()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public void deleteCategory(Long id) {
        this.categoryRepository.deleteById(id);
    }

}
