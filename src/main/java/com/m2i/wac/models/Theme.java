package com.m2i.wac.models;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;


/**
 * The persistent class for the themes database table.
 * 
 */
@Entity
@Table(name="themes")
@Data
public class Theme{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonProperty(access = Access.WRITE_ONLY)
	@CreationTimestamp
	@Column(name="date_create",updatable = false)
	private LocalDateTime dateCreate;

	@JsonProperty(access = Access.WRITE_ONLY)
	@UpdateTimestamp
	@Column(name="date_update")
	private LocalDateTime dateUpdate;

	@Column(name="date_start")
	private LocalDateTime dateStart;

	@Column(name="date_end")
	private LocalDateTime dateEnd;

	private String name;

	@ManyToOne
	@JoinColumn(name="id_periode")
	private Periode periode;
	
	@JsonIgnore
	@OneToMany(mappedBy="theme")
	private List<Art> arts;
	
	@Transient
	private Integer nbParticipation;
	
	public Integer getNbParticipation(){
		return arts.size();
	}

}