package com.m2i.wac.repositories;
import com.m2i.wac.models.Art;
import com.m2i.wac.models.Category;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ArtRepository extends JpaRepository<Art, Long> {

	public List<Art> getArtByThemeId(Long id);
	public List<Art> getArtByUserId(Long id);

	public Art getArtByThemeIdAndUserId(Long id, Long idUser);

	public List<Art> getArtByThemeIdAndCategoryId(Long idTheme, Long idCategory);
	public Collection<? extends Art> getArtByThemeIdAndCategoryIdAndTagsId(Long id, Long idCategory, Long idTag);
	public Collection<? extends Art> getArtByThemeIdAndTagsId(Long id, Long idTag);
	public List<Art> getArtByUserIdOrderByThemeIdDesc(Long id);

}
