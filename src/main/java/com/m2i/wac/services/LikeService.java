package com.m2i.wac.services;

import com.m2i.wac.models.Like;

import java.util.List;

public interface LikeService {
    public List<Like> getAll();

    public Like getById(Long id);

    public Like postLike(Like like);

    public Like putLike(Like like);

    public void delete(Long id);

	Boolean isAlreadyLiked(Like like);

	Boolean deleteByUserIdAndArtId(Like like);


}
