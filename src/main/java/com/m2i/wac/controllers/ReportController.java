package com.m2i.wac.controllers;

import com.m2i.wac.models.Report;
import com.m2i.wac.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("reports")
public class ReportController {

    @Autowired
    private ReportService reportService;

    @GetMapping
    public List<Report> getAll() { return this.reportService.getAll(); }

    @GetMapping("{id}")
    public Report getById(@PathVariable Long id) { return this.reportService.getById(id); }

    @PostMapping
    public Report postReport(@RequestBody Report report) { return this.reportService.postReport(report); }

    @PutMapping
    public Report putReport(@RequestBody Report report) { return this.reportService.putReport(report); }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) { this.reportService.delete(id); }

}

