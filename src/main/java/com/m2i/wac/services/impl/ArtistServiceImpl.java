package com.m2i.wac.services.impl;

import com.m2i.wac.models.Art;
import com.m2i.wac.models.SocialMedia;
import com.m2i.wac.models.Tag;
import com.m2i.wac.models.User;
import com.m2i.wac.models.dtos.ArtistDTO;
import com.m2i.wac.repositories.*;
import com.m2i.wac.services.ArtistService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

public class ArtistServiceImpl implements ArtistService {

    private UserRepository userRepository;
    private SocialMediaRepository socialMediaRepository;
    private ArtRepository artRepository;
    private FollowRepository followRepository;


    public ArtistServiceImpl(UserRepository userRepository, SocialMediaRepository socialMediaRepository, ArtRepository artRepository, FollowRepository followRepository) {
        this.userRepository = userRepository;
        this.socialMediaRepository = socialMediaRepository;
        this.artRepository = artRepository;
        this.followRepository = followRepository;
    }


    @Override
    public List<ArtistDTO> getAll(){
        List<User> users = this.userRepository.findAll();
        List<ArtistDTO> artists = new ArrayList<ArtistDTO>();

        for(User user : users){
            if(user.getRole().getName().equals("artist")){
                artists.add(this.getById(user.getId()));
            }
        }
        return artists;
    }

    @Override
    public ArtistDTO getById(Long id){

        // Récupération des informations utilisateur de l'artiste
        User user = this.userRepository.findById(id).orElseThrow(
                ()-> new ResponseStatusException(HttpStatus.NOT_FOUND));

        // Récupération de tous les réseaux sociaux de l'artiste
        List<SocialMedia> socialMedias = this.socialMediaRepository.getSocialMediaByUserId(id);

        // Récupération de toutes les oeuvres liées à l'artiste
        List<Art> arts = this.artRepository.getArtByUserIdOrderByThemeIdDesc(id);
        
        Art soumission = null;
        if(arts.size()>0&&arts.get(0).getTheme().getPeriode().getId()==1) {
        	soumission=arts.get(0);
        	arts.remove(0);
        }
        

        // Récupération de tout les tags liés aux oeuvres de l'artiste
        List<Tag> tags = new ArrayList<Tag>();
        for (Art art : arts){
            List<Tag> artTags = art.getTags();
            for (Tag artTag : artTags){
                if (!tags.contains(artTag)){
                    tags.add(artTag);
                }
            }
        }

        // Récupération du nb de participation de l'artiste
        Integer nbParticipation = (Integer) arts.size();

        // Récupération du nb de follow de l'artiste
        Integer nbFollow = (Integer) this.followRepository.getFollowByUserFollowedId(id).size();


        ArtistDTO artist = new ArtistDTO(
                user.getId(),
                user.getDescription(),
                user.getFirstname(),
                user.getLastname(),
                user.getMail(),
                user.getPseudo()
        );
        artist.setSocialMedias(socialMedias);
        artist.setArts(arts);
        artist.setTags(tags);
        artist.setNbParticipation(nbParticipation);
        artist.setNbFollow(nbFollow);
        artist.setSoumission(soumission);
        return artist;
    }

    @Override
    public void deleteArtist(Long id){
        this.userRepository.deleteById(id);
    }

    @Override
    public List<Art> getAllArts(Long id){
        List<Art> arts = this.artRepository.getArtByUserId(id);
        return arts;
    }

}
