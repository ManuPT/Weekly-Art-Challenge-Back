package com.m2i.wac.configs;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@Aspect
@EnableAspectJAutoProxy
public class AspectLoggerConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(AspectLoggerConfiguration.class);

    @Before("execution(* *..*Controller.*(..))")
    public void beforeGetMapping(JoinPoint jp){
        LOGGER.info("Appel de la methode" + jp.getSignature().toShortString());
    }

    @AfterThrowing("execution(* *..*ServiceImpl.*(..))")
    public void afterThrowingInService(JoinPoint jp) {
        LOGGER.warn("Erreur throw dans la méthode : " + jp.getSignature().toShortString());
    }
}