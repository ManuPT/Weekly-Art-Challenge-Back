package com.m2i.wac.services;

import com.m2i.wac.models.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();

    Category getById(Long id);

    Category postCategory(Category category);

    Category putCategory(Category category);

    void deleteCategory(Long id);
}
