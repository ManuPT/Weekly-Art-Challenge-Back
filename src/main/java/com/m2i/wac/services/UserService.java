package com.m2i.wac.services;

import java.util.List;

import com.m2i.wac.models.User;
import com.m2i.wac.models.dtos.UserLoginDTO;

public interface UserService {

	List<User> getAll();

	User getById(Long id);

	User post(User user);

	User put(User user);

	void delete(Long id);

	User login(UserLoginDTO user);

}
