package com.m2i.wac.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.m2i.wac.models.User;
import com.m2i.wac.models.dtos.UserLoginDTO;
import com.m2i.wac.services.UserService;

@RestController
@CrossOrigin
@RequestMapping("users")
public class UserController {
	@Autowired
	private UserService userService;
	
	@GetMapping
	public List<User> getAll(){
		return this.userService.getAll();
	}
	
	@GetMapping("{id}")
	public User getById(@PathVariable Long id){
		return this.userService.getById(id);
	}
	
	@PostMapping
	public User postUser(@RequestBody User user) {
		return this.userService.post(user);
	}
	
	@PutMapping
	public User putUser(@RequestBody User user) {
		return this.userService.put(user);
	}
	
	@DeleteMapping("{id}")
	public void deleteUser(@PathVariable Long id) {
		this.userService.delete(id);
	}
	
	@PostMapping("login")
	public User login(@RequestBody UserLoginDTO user) {
		return this.userService.login(user);
	}
	
}
