# Weekly Art Challenge

The best online art challenge ! 


## Lancer le projet
Dans le dossier "wac_db" :  créer un dossier "data".  
Depuis le dossier "wac_db" : taper la commande "docker compose up".  
Depuis le répertoire racine du projet : taper la commande "mvn install" (seulement pour la première execution).  
Editer le fichier de configuration "src/main/resources/application.yml" (pour la connexion bdd).  
Depuis le répertoire racine du projet : taper la commande "mvn spring-boot:run" (pour lancer le serveur).  


## Routes API
* [Art](#art)
* [Artist](#artist)
* [Category](#category)
* [Like](#like)
* [Report](#report)
* [Social Media](#social-media)
* [Tag](#tag)
* [Theme](#theme)
* [User](#user)


### ART

#### GET /arts/{id}

<details>
<summary>Structure de retour exemple :</summary>

	{
        "id": 14,  
		"content": "htpp://peutetre",  
		"name": "rework incroyable en PUT 2",  
		"category": {  
			"id": 1,   
			"name": "musique"  
		},
		"theme": {  
			"id": 1,    
			"dateEnd": "2021-09-04T00:00:01.000+00:00",  
			"dateStart": "2021-08-30T00:00:01.000+00:00",   
			"name": "toto à la plage",  
			"periode": {
				"id": 2,
				"name": "résultat"
			}
		},
		"user": {
			"id": 1,
			"description": "je suis un petit coco",
			"firstname": "coco",
			"lastname": "des iles",
			"mail": "coco.des.iless@yopmail.com",
			"password": "coco97",
			"pseudo": "coco97",
			"role": {
				"id": 3,
				"name": "user"
			}
		},
		"tags": [
			{
				"id": 1,
				"name": "black & white"
			},
			{
				"id": 2,
				"name": "music"
			}
		],
		"nbrLikes": 0,
        "nbrLikesToday": 0
	}

</details>


#### GET /arts

Même structure que GET /arts/{id} mais en liste.

#### POST /arts

<details>
<summary>Exemple de données attendues:</summary>

	{
		"id": 14,
		"content": "htpp://peutetre",
		"name": "rework incroyable en PUT 2",
		"category": {
			"id": 1
		},
		"user": {
			"id": 1
		},
		"tags": [
			{"id":1},
			{"id":2}
		]
	}
</details>

#### PUT /arts

Même structure que le PUT /arts mais avec l'id de l'art à modifier.

#### DELETE /arts/{id}

Supprime l'art avec l'id correspondant.


---
### ARTIST

#### GET /artistes/{id}

<details>
<summary>Structure de retour exemple :</summary>

    {
        "id": 7,
        "description": "je suis le plus grand photographe de ma catégorie",
        "firstname": "Jean",
        "lastname": "rené",
        "mail": "jean.rene@yopmail.com",
        "pseudo": "JR",
        "tags": [
            {
                "id": 6,
                "name": "painting"
            },
            {
                "id": 9,
                "name": "photography"
            },
            {
                "id": 1,
                "name": "black & white"
            }
        ],
        "arts": [
            {
                "id": 9,
                "content": "https://images.pexels.com/photos/2154502/pexels-photo-2154502.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
                "name": "le vert c'est trop trop cool",
                "category": {
                    "id": 3,
                    "name": "peinture"
                },
                "theme": {
                    "id": 3,
                    "dateStart": "2021-09-13T02:00:01",
                    "dateEnd": "2021-09-18T02:00:01",
                    "name": "les petites choses",
                    "periode": {
                        "id": 2,
                        "name": "résultat"
                    }
                },
                "user": {
                    "id": 7,
                    "description": "je suis le plus grand photographe de ma catégorie",
                    "firstname": "Jean",
                    "lastname": "rené",
                    "mail": "jean.rene@yopmail.com",
                    "pseudo": "JR",
                    "role": {
                        "id": 2,
                        "name": "artist"
                    }
                },
                "tags": [
                    {
                        "id": 9,
                        "name": "photography"
                    }
                ],
                "nbrLikes": 3,
                "nbrLikesToday": 0
            },
            {
                "id": 12,
                "content": "https://images.pexels.com/photos/2279334/pexels-photo-2279334.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
                "name": "star war et earth peace",
                "category": {
                    "id": 3,
                    "name": "peinture"
                },
                "theme": {
                    "id": 4,
                    "dateStart": "2021-09-20T02:00:01",
                    "dateEnd": "2021-09-25T02:00:01",
                    "name": "start peace",
                    "periode": {
                        "id": 1,
                        "name": "vote"
                    }
                },
                "user": {
                    "id": 7,
                    "description": "je suis le plus grand photographe de ma catégorie",
                    "firstname": "Jean",
                    "lastname": "rené",
                    "mail": "jean.rene@yopmail.com",
                    "pseudo": "JR",
                    "role": {
                        "id": 2,
                        "name": "artist"
                    }
                },
                "tags": [
                    {
                        "id": 9,
                        "name": "photography"
                    }
                ],
                "nbrLikes": 2,
                "nbrLikesToday": 0
            },
            {
                "id": 15,
                "content": "https://images.pexels.com/photos/1366909/pexels-photo-1366909.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
                "name": "Montagne! Jusqu'où vous arrêtez-vous !",
                "category": {
                    "id": 3,
                    "name": "peinture"
                },
                "theme": {
                    "id": 5,
                    "dateStart": "2021-09-27T02:00:01",
                    "dateEnd": "2021-10-02T02:00:01",
                    "name": "les dérives des continents",
                    "periode": {
                        "id": 1,
                        "name": "vote"
                    }
                },
                "user": {
                    "id": 7,
                    "description": "je suis le plus grand photographe de ma catégorie",
                    "firstname": "Jean",
                    "lastname": "rené",
                    "mail": "jean.rene@yopmail.com",
                    "pseudo": "JR",
                    "role": {
                        "id": 2,
                        "name": "artist"
                    }
                },
                "tags": [
                    {
                        "id": 9,
                        "name": "photography"
                    }
                ],
                "nbrLikes": 3,
                "nbrLikesToday": 0
            }
        ],
        "socialMedias": [
            {
                "id": 12,
                "link": "https://www.linkedin.com/in/jr-artiste-63b614137/?originalSubdomain=fr",
                "logo": {
                    "id": 1,
                    "logo": "https://cdn-icons-png.flaticon.com/512/174/174857.png",
                    "name": "linkedin"
                },
                "user": {
                    "id": 7,
                    "description": "je sui le plus grand photographe de ma catégorie",
                    "firstname": "Jean",
                    "lastname": "rené",
                    "mail": "jean.rene@yopmail.com",
                    "pseudo": "JR",
                    "role": {
                        "id": 2,
                        "name": "artist"
                    }
                }
            },
            {
                "id": 13,
                "link": "https://fr-fr.facebook.com/JRartiste/",
                "logo": {
                    "id": 2,
                    "logo": "https://cdn-icons-png.flaticon.com/512/733/733547.png",
                    "name": "facebook"
                },
                "user": {
                    "id": 7,
                    "description": "je sui le plus grand photographe de ma catégorie",
                    "firstname": "Jean",
                    "lastname": "rené",
                    "mail": "jean.rene@yopmail.com",
                    "pseudo": "JR",
                    "role": {
                        "id": 2,
                        "name": "artist"
                    }
                }
            },
            {
                "id": 14,
                "link": "https://twitter.com/JRart?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor",
                "logo": {
                    "id": 3,
                    "logo": "https://cdn-icons-png.flaticon.com/512/124/124021.png",
                    "name": "twitter"
                },
                "user": {
                    "id": 7,
                    "description": "je sui le plus grand photographe de ma catégorie",
                    "firstname": "Jean",
                    "lastname": "rené",
                    "mail": "jean.rene@yopmail.com",
                    "pseudo": "JR",
                    "role": {
                        "id": 2,
                        "name": "artist"
                    }
                }
            },
        ],
        "nbParticipation": 8,
        "nbFollow": 3
    }

</details>

#### GET /artistes

Même structure que GET /artistes/{id} mais en liste.

#### GET /artistes/arts/{id}

Retourne la liste des Art d'un artiste (voir structure GET /arts).

#### DELETE /artistes/{id}

Supprime l'artiste (l'utilisateur) avec l'id correspondant.


---
### CATEGORY

#### GET /categories/{id}

<details>
<summary>Structure de retour exemple :</summary>

    {
    	"id": 2,
    	"name": "Photographie"
    }
</details>

#### GET /categories

Même retour que /categories/{id} mais en liste.

#### POST /categories

<details>
<summary>Exemple de données attendues:</summary>

	{
    	"name":"Sculpture"
	}
</details>

#### PUT /categories

Même structure que POST mais avec un id existant en plus.

#### DELETE /categories/{id}

Supprime la categorie avec l'id correspondant.


---
### LIKE

#### GET /likes/{id}

<details>
<summary>Structure de retour exemple :</summary>

    {
        "id": 7,
        "art": {
            "id": 13,
            "content": "https://www.dico-poesie.com/reference-poeme/332/Le_deluge.php",
            "name": "Le déluge",
            "category": {
                "id": 2,
                "name": "poésie"
            },
            "theme": {
                "id": 5,
                "dateStart": "2021-09-27T02:00:01",
                "dateEnd": "2021-10-02T02:00:01",
                "name": "les dérives des continents",
                "periode": {
                    "id": 1,
                    "name": "vote"
                }
            },
            "user": {
                "id": 5,
                "description": "Je suis le plus grand danseur de ma catégorie",
                "firstname": "Ju",
                "lastname": "Dorémifasol",
                "mail": "j.golden@yopmail.com",
                "pseudo": "Godloju",
                "role": {
                    "id": 2,
                    "name": "artist"
                }
            },
            "tags": [
                {
                    "id": 3,
                    "name": "poetry"
                }
            ],
            "nbrLikes": 2,
            "nbrLikesToday": 0
        },
        "user": {
            "id": 1,
            "description": "je suis un petit coco",
            "firstname": "coco",
            "lastname": "des iles",
            "mail": "coco.des.iless@yopmail.com",
            "pseudo": "coco97",
            "role": {
                "id": 3,
                "name": "user"
            }
        }
    }
</details>

#### GET /likes

Même retour que /likes/{id} mais en liste.

#### POST /likes

<details>
<summary>Exemple de données attendues:</summary>

    {
        "user":{
            "id":4
        },
        "art":{
            "id":22
        }
    }
</details>

#### PUT /likes

Même structure que POST mais avec un id existant en plus.

#### DELETE /likes/{id}

Supprime le tags avec l'id correspondant.


---
### REPORT

#### GET /reports/{id}

<details>
<summary>Structure de retour exemple :</summary>

    {
        "id": 1,
        "art": {
            "id": 2,
            "content": "https://images.pexels.com/photos/1509534/pexels-photo-1509534.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
            "name": "le bleu c'est cool",
            "category": {
                "id": 3,
                "name": "peinture"
            },
            "theme": {
                "id": 1,
                "dateStart": "2021-08-30T02:00:01",
                "dateEnd": "2021-09-04T02:00:01",
                "name": "blue",
                "periode": {
                    "id": 2,
                    "name": "résultat"
                }
            },
            "user": {
                "id": 7,
                "description": "je suis le plus grand photographe de ma catégorie",
                "firstname": "Jean",
                "lastname": "rené",
                "mail": "jean.rene@yopmail.com",
                "pseudo": "JR",
                "role": {
                    "id": 2,
                    "name": "artist"
                }
            },
            "tags": [
                {
                    "id": 6,
                    "name": "painting"
                }
            ],
            "nbrLikes": 1,
            "nbrLikesToday": 0
        },
        "user": {
            "id": 1,
            "description": "je suis un petit coco",
            "firstname": "coco",
            "lastname": "des iles",
            "mail": "coco.des.iless@yopmail.com",
            "pseudo": "coco97",
            "role": {
                "id": 3,
                "name": "user"
            }
        }
    }
</details>

#### GET /reports

Même retour que /reports/{id} mais en liste.

#### POST /reports

<details>
<summary>Exemple de données attendues:</summary>

    {
        "art":{
            "id":11
        },
        "user":{
            "id":3
        }
    }
</details>

#### PUT /reports

Même structure que POST mais avec un id existant en plus.

#### DELETE /reports/{id}

Supprime le report avec l'id correspondant.


---
### SOCIAL MEDIA

#### GET /social-medias/{id}

<details>
<summary>Structure de retour exemple :</summary>

    {
        "id": 2,
        "link": "https://fr-fr.facebook.com/juliendoreofficiel",
        "logo": {
            "id": 2,
            "logo": "https://cdn-icons-png.flaticon.com/512/733/733547.png",
            "name": "facebook"
        },
        "user": {
            "id": 5,
            "description": "Je suis le plus grand danseur de ma catégorie",
            "firstname": "Ju",
            "lastname": "Dorémifasol",
            "mail": "j.golden@yopmail.com",
            "pseudo": "Godloju",
            "role": {
                "id": 2,
                "name": "artist"
            }
        }
    }
</details>

#### GET /social-medias

Même retour que /social-medias/{id} mais en liste.

#### POST /social-medias

<details>
<summary>Exemple de données attendues:</summary>

    {
        "link": "https://www.linkedin.com/in/EdithPiaf",
        "logo":{
            "id":1
        },
        "user":{
            "id":4
        }
    }
</details>

#### PUT /social-medias

Même structure que POST mais avec un id existant en plus.

#### DELETE /social-medias/{id}

Supprime le social media avec l'id correspondant.


---
### TAG

#### GET /tags/{id}

<details>
<summary>Structure de retour exemple :</summary>

    {
    	"id": 2,
    	"name": "Bleu"
    }
</details>

#### GET /tags

Même retour que /tags/{id} mais en liste.

#### POST /tags

<details>
<summary>Exemple de données attendues:</summary>

	{
    	"name":"Bleu"
	}
</details>

#### PUT /tags

Même structure que POST mais avec un id existant en plus.

#### DELETE /tags/{id}

Supprime le tag avec l'id correspondant.


---
### THEME

#### GET /themes/{id}


<details>
<summary>Structure de retour exemple :</summary>

    {
        "id": 5,
        "dateStart": "2021-09-27T02:00:01",
        "dateEnd": "2021-10-02T02:00:01",
        "name": "les dérives des continents",
        "periode": {
			"id": 1,
			"name": "vote"
        }
    }
</details>

#### GET /themes

Même retour que /themes/{id} mais en liste.

#### POST /themes

<details>
<summary>Exemple de données attendues:</summary>

    {
        "dateStart":"2021-09-28T00:00:00",
        "dateEnd":"2021-10-04T00:00:00",
        "name":"Banane",
        "periode":{
        "id":1
        }
    }
</details>

#### PUT /themes

Même structure que POST mais avec un id existant en plus.

#### DELETE /themes/{id}

Supprime le theme avec l'id correspondant.  
Ne supprime QUE les themes n'ayant pas été assigné à une oeuvre.

#### GET /themes/{id}/arts

Retourne la liste des Art de ce thème (voir structure GET /arts).

#### GET /themes/next

Retourne le Theme en état actuel de soumission (voir structure GET /themes/{id}).

#### GET /themes/next/arts

Retourne la liste des Art du thème en état actuel de soumission (voir structure GET /arts).

#### GET /themes/current

Retourne le Theme en état actuel de vote (voir structure GET /themes/{id}).

#### GET /themes/current/arts

Retourne la liste des Art en état actuel de vote (voir structure GET /arts).

#### GET /themes/current/arts/followed/{idUser}

Retourne la liste des Art en état actuel de vote que le User follow (voir structure GET /arts).

#### GET /themes/current/arts/daily

Retourne la liste des 10 Art en état actuel de vote qui sont le plus votées sur la journée (voir structure GET /arts).

#### GET /themes/last

Retourne le dernier Theme en état de résultat (voir structure GET /themes/{id}).

#### GET /themes/last/arts

Retourne la liste des Art du dernier Theme en état de résultat (voir structure GET /arts).

#### GET /themes/last/arts/followed/{idUser}

Retourne la liste des Art du dernier Theme en état de résultat que le User follow (voir structure GET /arts).

#### GET /themes/last/arts/categories/{idCategory}/top10

Retourne la liste des 10 Art du dernier Theme en état de résulat ayant le plus de Like et appartenant à la Category (voir structure GET /arts).  

#### GET /themes/{id}/arts/tags  

Retourne la liste des tags d'un thème suivant tout les Art posté (voir structure GET /tags).  

#### POST /themes/{id}/arts/sort  

Retour une liste des Art suivant le Theme, la Category (optionnelle) , et une List de Tags  

<details>
<summary>Exemple de données attendues:</summary>

    {
        "category":{"id":9},
        "tags":[
			{"id":1},
			{"id":2}
		]
	}
</details>


---
### USER

#### GET /users/{id}


<details>
<summary>Structure de retour exemple :</summary>

	{
		"id": 5,
		"description": "Je suis le plus grand danseur de ma catégorie",
		"firstname": "pierre",
		"lastname": "de ladance",
		"mail": "pierre.dance@yopmail.com",
		"pseudo": "pierredance",
		"role": {
			"id": 2,
			"name": "artist"
		}
	}
</details>
	
#### GET /users

Même retour que /users/{id} mais en liste.

#### POST /users

<details>
<summary>Exemple de données attendues:</summary>

    {
        "pseudo": "testpseudo",
        "password": "il va etre hasher tqt",
        "mail": "test.dance@yopmail.com",
        "firstname": "test",
        "lastname": "test",
        "description": "Je suis le plus grand test de ma catégorie"
    }
</details>

#### PUT /users

Même structure que POST mais avec un id existant en plus.

#### DELETE /users/{id}

Supprime le user avec l'id correspondant.

#### POST /users/login

Permet aux utilisateurs du site de s'authentifier.  
<details>
<summary>Exemple de données attendues:</summary>
    
    {
        "pseudo": "testpseudo",
        "password": "il va etre hasher tqt"
    }
</details>