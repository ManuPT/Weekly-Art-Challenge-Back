package com.m2i.wac.services;

import com.m2i.wac.models.Art;
import com.m2i.wac.models.dtos.ArtistDTO;

import java.util.List;

public interface ArtistService {

    public List<ArtistDTO> getAll();
    public ArtistDTO getById(Long id);
    public void deleteArtist(Long id);
    public List<Art> getAllArts(Long id);
}
