package com.m2i.wac.services;


import com.m2i.wac.models.Art;
import com.m2i.wac.models.Tag;
import com.m2i.wac.models.Theme;
import com.m2i.wac.models.dtos.SortDTO;

import java.util.HashSet;
import java.util.List;

public interface ThemeService {

    public List<Theme> getAll();
    public Theme postTheme(Theme theme);
    public Theme putTheme(Theme theme);
    public Theme getById(Long id);
    public void deleteTheme(Long id);
	public Theme getNextTheme();
	public Theme getCurrentTheme();
	public Theme getLastTheme();
	public List<Art> getArtByCurrentTheme();
	public List<Art> getArtByThemeId(Long id);
	public List<Art> getArtByNextTheme();
	public List<Art> getArtByLastTheme();
	public List<Art> getArtByCurrentVoteThemeAndFollowedByUserId(Long id);
	public List<Art> getArtByCurrentVoteThemeMostLikedToday();
	public List<Art> getArtByLastThemeAndFollowedByUserId(Long id);
	public List<Art> getArtByLastThemeAndTop10ByIdCategory(Long idCategory);
	public HashSet<Art> getArtByThemeIdAndSortDTO(Long id, SortDTO sort);
	public HashSet<Tag> getTagByThemeId(Long id);
}
