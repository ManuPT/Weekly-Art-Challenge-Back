package com.m2i.wac.repositories;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.m2i.wac.models.Theme;

public interface ThemeRepository extends JpaRepository<Theme, Long>{

	public Theme getThemeByPeriodeId(Long id);

	public Theme findFirstThemeByPeriodeIdOrderByDateStartDesc(Long id);

	public List<Theme> findAllByOrderByDateStartDesc();

}
