package com.m2i.wac.services;

import java.util.List;

import com.m2i.wac.models.Tag;

public interface TagService {

	public List<Tag> getAll();

	public Tag getById(Long id);

	public Tag post(Tag tag);

	public Tag put(Tag tag);

	public void delete(Long id);

}
