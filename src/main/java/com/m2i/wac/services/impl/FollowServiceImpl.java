package com.m2i.wac.services.impl;

import com.m2i.wac.models.Follow;
import com.m2i.wac.repositories.FollowRepository;
import com.m2i.wac.services.FollowService;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

public class FollowServiceImpl implements FollowService {

    private FollowRepository followRepository;

    public FollowServiceImpl(FollowRepository followRepository) {
        this.followRepository = followRepository;
    }

    @Override
    public Follow getById(Long id) {
        return this.followRepository.findById(id).orElseThrow(
                ()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public Follow postFollow(Follow follow) {
        return this.followRepository.save(follow);
    }

    @Override
    public Follow putFollow(Follow follow) {
        return this.followRepository.save(follow);
    }

    @Override
    public void deleteFollow(Long id) {
        this.followRepository.deleteById(id);
    }

    @Override
    public List<Follow> findAll() {
        return this.followRepository.findAll();
    }

    @Override
    public List<Follow> findFollowsByUserId(Long id) {
        return this.followRepository.findByUserId(id);
    }

    public Boolean isAlreadyFollowed(Follow follow) {
        Long userId = follow.getUser().getId();
        Long userFollowedId = follow.getUserFollowed().getId();

        return this.followRepository.existsByUserIdAndUserFollowedId(userId, userFollowedId);
    }

    @Override
    public Boolean deleteByUserIdAndUserFollowedId(Follow follow) {
        Long userId = follow.getUser().getId();
        Long userFollowedId = follow.getUserFollowed().getId();
        Follow result = this.followRepository.findByUserIdAndUserFollowedId(userId, userFollowedId).orElseThrow(
                        ()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
        this.followRepository.deleteById(result.getId());
        return true;
    }
}
