package com.m2i.wac.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.m2i.wac.models.Tag;
import com.m2i.wac.services.TagService;

@RestController
@CrossOrigin
@RequestMapping("tags")
public class TagController {
	@Autowired
	private TagService tagService;
	
	@GetMapping
	public List<Tag> getAll(){
		return this.tagService.getAll();
	}
	
	@GetMapping("{id}")
	public Tag getAll(@PathVariable Long id){
		return this.tagService.getById(id);
	}
	
	@PostMapping
	public Tag post(@RequestBody Tag tag) {
		return this.tagService.post(tag);
	}
	
	@PutMapping
	public Tag put(@RequestBody Tag tag) {
		return this.tagService.put(tag);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable Long id) {
		this.tagService.delete(id);
	}
	
}
