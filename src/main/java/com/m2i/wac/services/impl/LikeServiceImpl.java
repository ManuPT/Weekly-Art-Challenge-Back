package com.m2i.wac.services.impl;

import com.m2i.wac.models.Follow;
import com.m2i.wac.models.Like;
import com.m2i.wac.repositories.LikeRepository;
import com.m2i.wac.services.LikeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

public class LikeServiceImpl implements LikeService {

    private LikeRepository likeRepository;

    public LikeServiceImpl(LikeRepository likeRepository) {
        this.likeRepository = likeRepository;
    }

    @Override
    public List<Like> getAll() { return this.likeRepository.findAll();  }

    @Override
    public Like getById(Long id) {
        return this.likeRepository.findById(id).orElseThrow(
                ()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public Like postLike(Like like) { return this.likeRepository.save(like); }

    @Override
    public Like putLike(Like like) { return this.likeRepository.save(like); }

    @Override
    public void delete(Long id) {this.likeRepository.deleteById(id); }
    
    @Override
    public Boolean isAlreadyLiked(Like like) {
        Long userId = like.getUser().getId();
        Long artId = like.getArt().getId();

        return this.likeRepository.existsByUserIdAndArtId(userId, artId);
    }

    @Override
    public Boolean deleteByUserIdAndArtId(Like like) {
        Long userId = like.getUser().getId();
        Long artId = like.getArt().getId();
        Like result = this.likeRepository.findByUserIdAndArtId(userId, artId).orElseThrow(
                        ()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
        this.likeRepository.deleteById(result.getId());
        return true;
    }
}
