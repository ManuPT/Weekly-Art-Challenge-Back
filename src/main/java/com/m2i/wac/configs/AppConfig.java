package com.m2i.wac.configs;

import com.m2i.wac.repositories.*;
import com.m2i.wac.services.*;

import com.m2i.wac.services.impl.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
	@Bean
	public ArtService artService(ArtRepository artRepository,ThemeRepository themeRepository) {
		return new ArtServiceImpl(artRepository,themeRepository);
	}

	@Bean
	public CategoryService categoryService(CategoryRepository categoryRepository){
		return new CategoryServiceImpl(categoryRepository);
	}

	@Bean
	public UserService userService(UserRepository userRepository) {
		return new UserServiceImpl(userRepository);
	}
	
	@Bean
	public TagService tagService(TagRepository tagRepository) {
		return new TagServiceImpl(tagRepository);
	}

	@Bean
	public ThemeService themeService(ThemeRepository themeRepository,ArtRepository artRepository,FollowRepository followRepository){
		return new ThemeServiceImpl(themeRepository,artRepository,followRepository);
	}

	@Bean
	public ReportService reportService(ReportRepository reportRepository) {
		return new ReportServiceImpl(reportRepository);
	}

	@Bean
	public LikeService likeService(LikeRepository likeRepository) {
		return new LikeServiceImpl(likeRepository);
	}

	@Bean
	public SocialMediaService socialMediaService(SocialMediaRepository socialMediaRepository) {
		return new SocialMediaServiceImpl(socialMediaRepository);
	}

	@Bean
	public ArtistService artistService(UserRepository userRepository, SocialMediaRepository socialMediaRepository, ArtRepository artRepository, FollowRepository followRepository){
		return new ArtistServiceImpl(userRepository, socialMediaRepository, artRepository, followRepository);
	}

	@Bean
	public FollowService followService(FollowRepository followRepository) {
		return new FollowServiceImpl(followRepository);
	}
}
