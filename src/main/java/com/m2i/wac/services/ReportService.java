package com.m2i.wac.services;

import com.m2i.wac.models.Report;

import java.util.List;

public interface ReportService {
    public List<Report> getAll();

    public Report getById(Long id);

    public Report postReport(Report report);

    public Report putReport(Report report);

    public void delete(Long id);
}
