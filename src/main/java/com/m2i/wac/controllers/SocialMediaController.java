package com.m2i.wac.controllers;

import com.m2i.wac.models.SocialMedia;
import com.m2i.wac.services.SocialMediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("social-medias")
public class SocialMediaController {

    @Autowired
    private SocialMediaService socialMediaService;

    @GetMapping
    public List<SocialMedia> getAll() { return this.socialMediaService.getAll(); }

    @GetMapping("{id}")
    public SocialMedia getbyId(@PathVariable Long id) { return this.socialMediaService.getById(id); }

    @PostMapping
    public SocialMedia postSocialMedia(@RequestBody SocialMedia socialMedia) { return this.socialMediaService.postSocialMedia(socialMedia); }

    @PutMapping
    public SocialMedia putSocialMedia(@RequestBody SocialMedia socialMedia) { return this.socialMediaService.putSocialMedia(socialMedia); }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) { this.socialMediaService.delete(id); }
}
