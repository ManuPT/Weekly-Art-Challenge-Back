package com.m2i.wac.services;

import com.m2i.wac.models.Follow;

import java.util.List;

public interface FollowService {
    Follow getById(Long id);

    Follow postFollow(Follow follow);

    Follow putFollow(Follow follow);

    void deleteFollow(Long id);

    List<Follow> findAll();

    List<Follow> findFollowsByUserId(Long id);

    Boolean isAlreadyFollowed(Follow follow);

    Boolean deleteByUserIdAndUserFollowedId(Follow follow);
}
